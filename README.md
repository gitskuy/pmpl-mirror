# 1506722752-practice

Ahmad Yazid - 1506722752 - Computer Science 2015 - PMPL B

access mirror repository:
https://gitlab.com/gitskuy/pmpl-mirror

access heroku:
https://ahmadyazidhomepage.herokuapp.com/

# Cerita Exercise 3

Cerita akan exercise 3

## Test Isolation

Menurut parcival ketika kita melakukan functional test yang tidak terisolasi kita akan meningalkan list item yang akan tersia-sia ada pada data base,dan ini bisa membuat test kita dimasa depan tergangu akan hal ini. Dengan mengunakan metode baru yang kita implementasikan kita bisa membenarkan funtional test kita sehinga hal ini tidak akan terjadi pada masa depan.

Dengan mengunakan metode yang kita implementasikan pada exercise ketiga, kita bisa pastikan bahwa test-test kita tidak akan menganggu test yang lain.

## Perbedaan code

### Code Lama

Bisa dilihat perbedaan nya ialah kita sekarang memakai liveserver url yang memungkinkan kan kita untuk melaukakan test secara independen. Bisa dilihat dikarenakan kita memakai implementasi ini sekarang kita bisa melakukan test jikalau ada dua orang yang berbeda mengakses situs tersebut.

```python
    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://localhost:8000')
        header_text = self.browser.find_element_by_id('to_do_header').text
        self.assertIn('To-Do', header_text)

```

### Code Baru

```python
 def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get(self.live_server_url)
        header_text = self.browser.find_element_by_id('to_do_header').text
        self.assertIn('To-Do', header_text)

```

# Cerita BAB 7

Untuk bab 7 kami melakukan preetifying dengan mengunakkan css framework populer yaitu boot strap!

Untuk perubahan kode sangat lah banyak namun yang paling signifikan bisa dilihat pada bagaimana kita menstrukturkan html yang akan di render kita sekarang memakai templates yang akan memudahkan kedepan nya.

### Code Lama

Bisa dilihat perbedaan nya ialah kita sekarang memakai liveserver url yang memungkinkan kan kita untuk melaukakan test secara independen. Bisa dilihat dikarenakan kita memakai implementasi ini sekarang kita bisa melakukan test jikalau ada dua orang yang berbeda mengakses situs tersebut.

```html
<html>

<head>
<title>Ahmad Yazid Homepage</title>
<h1>Ahmad Yazid</h1>
<h2>1506722752</h2>
<head>

<body>
  <h1 id="to_do_header">To-Do</h1>
  <h2>Your To-Do list</h2>
  <form method="POST" action="/lists/new">
    <input name="item_text" id="id_new_item" placeholder="Enter a to-do item"/>
    {% csrf_token %}
    </form>
    <table id="id_list_table">
        {% for item in items %}
          <tr><td>{{forloop.counter}}: {{ item.text }} </td></tr>
        {% endfor %}
    </table>
    <h1 id="comment">{{comment}}</h1>
</body>
</html>

```

### Code Baru

pada homepage

```html
{% extends 'base.html' %} {% block to_do_header %}Start a new To-Do list{%
endblock %} {% block form_action %}/lists/new{% endblock %}
```

pada base.html(atau template awal)

```html
<html lang="en">

<head>
<div class="text-center">
<title>Ahmad Yazid Homepage</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="/static/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <link href="/static/base.css" rel="stylesheet" media="screen">
  <h1>Ahmad Yazid</h1>
  <h2>1506722752</h2>
</div>
<head>

<body>

  <div class="container">


     <div class="row">
        <div class="col-sm jumbotron">
          <div class="text-center">
           <h1 id="to_do_header">{% block to_do_header %}{% endblock %}</h1>
              <form method="POST" action="{% block form_action %}{% endblock %}">
              <input name="item_text" id="id_new_item" class="form-control input-lg" placeholder="Enter a to-do item"/>
                {% csrf_token %}
              </form>
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm">
            {% block table %}
            {% endblock %}
        </div>
          {% block comment %}
          {% endblock %}
      </div>

</div>
</body>


</html>



```

Untuk testing nya kita pun sekarang menganti dengan mengunakan StaticLiveServerCase dikarenakan sekarang file file kita sudah static namun dikarenakan untuk StaticLiveServerCase sudah terdeprikasi pada django terbaru kita mengunakan staticliveservertestcase

# Keterkaitan penerapan (RED,GREEN,REFRACTOR) dengan konsep clean code

Menurut buku obey the testing goat, dikarenakan banyak kritik yang menyarankan bahwa TDD akan membuat sebuah kode yang tak terarahkan, dikarenakan developer hanya fokus untuk membuat test yang dibuat untuk pass dan tidak melihat bahwa bagaimana sebuah sistem seharusnya di design (yang harry parcival bilang sedikit kurang logis). Itulah mengapa RGR ini dibutuhkan.

Kita harus menyadari bahwa TDD bukan lah hal yang sakti. Kita harus masih memikirkan akan design yang baik. Namun dikala kita melupakan "Refractor" pada RGR. Kita hanya akan membuat test - kode -test -kode tanpa melihat dan memantain design kode yang baik. Jikalau kita lalai dengan melakukan RGR ini dengan baik khususnya dibagian REFRACTOR sangat mudah nanti nya kita akan mengakumulasi hutang teknis (Technical Debt)

Namun memang terkadang ide brilian untuk merefractor kode tidak datang dengan sebegitunya. Kadang kita tidak tahu kapan ide itu akan muncul. Namun jika ide itu muncul dikala kita sedang melakukan sesuatu apakah kita harus stop semua yang kita lakukan dan langsung merefractor langsung?

Jawaban nya ialah tentu tergantung kita harus tau situasi juga! saat mau melakukan hal-hal tertentu pada kode kita.

Namun inti nya ialah kita tidak boleh melupakan RGR ini dikarenakan ini lah dasar-dasar yang bisa memastikan kode kita akan mencadi se bersih mungkin!

# Keuntungan Test Organization

Untuk keuntungan test organization menurut saya ada beberapa. Selain membuat kode kita terlihat lebih terstruktur dan lebih bersih. Keuntungan yang sangat signifikan ialah supaya kita bisa menjalankan test secara independen dan skala kecil. Dikarenakan lama kelamaan kita punya test akan banyak. Dan pada akhir nya kita butuh waktu yang lama jikalau kita harus melakukan test nya semua satu persatu. Itulah mengapa menurut saya test orginazation ini dibutuhkan.

Jadi untuk di conclusikan test orginazation membersihkan kode,memberi struktur pada kode dan juga bisa membuat aplikasi kita lebih scallable untuk masa depan.

# Mutation Testing

Buat 2 mutant dari fungsi pada views.py yang mewujudkan fitur tersebut

```python
    # First Mutant change =0 to != 0
    if count == 0:
        comment = "yey, waktunya berlibur"
    # Second Mutant change <5 to <= 5
    elif count < 5:
        comment = "sibuk tapi santai"
    else:
        comment = "oh tidak"
    return comment
```

Buat/ubah test cases yang dapat memberikan “Strongly kill” pada mutant yang kalian buat lalu
ceritakan pada README.md. Bilamana hal tersebut tidak bisa dilakukan, jelaskan mengapa dan
tuliskan di README.md.

```python
  def test_kill_firstmutant(self):
        request = HttpRequest()
        new_list = List.objects.create()
        Item.objects.create(text='itemey 1', list=new_list)
        Item.objects.create(text='itemey 2', list=new_list)
        Item.objects.create(text='itemey 3', list=new_list)
        Item.objects.create(text='itemey 4', list=new_list)
        Item.objects.create(text='itemey 5', list=new_list)
        Item.objects.create(text='itemey 6', list=new_list)
        response = self.client.get('/lists/%d/' % (new_list.id,))
        self.assertIn(b'<h1 id="comment">oh tidak</h1>',
                      response.content)

    def test_kill_secondMutant(self):
        request = HttpRequest()
        new_list = List.objects.create()
        Item.objects.create(text='itemey 1', list=new_list)
        Item.objects.create(text='itemey 2', list=new_list)
        Item.objects.create(text='itemey 3', list=new_list)
        Item.objects.create(text='itemey 4', list=new_list)
        Item.objects.create(text='itemey 5', list=new_list)
        response = self.client.get('/lists/%d/' % (new_list.id,))
        self.assertIn(b'<h1 id="comment">oh tidak</h1>',
                      response.content)

```

Bisa dilihat semua bahwa mutant pertama dibunuh oleh test_kill_firstmutant dan mutant kedua di kill oleh test_kill_secondMutant. Membunuh mutant satu dengan memasukan lebih dari 5 input yang akan membuat output yey waktunya libur namun yang keluar adalah oh tidak. Untuk membunuh yang kedua kita harus memasukan pas lima yang akan nanti membuat mutant teresbut mengeluarkan sibuk namun santai namun seharusnya lagi lagi yang keluar itulah oh tidak.
Namun jika kita lihat beberapa test saya sebelumnya sudah mengkill beberapa mutant yang terjadi jadi untuk commit kedepan nya saya akan merefractor kode yang sudah mengkill mutant tersebut.

## MUTPY

FAILED TO IMPLEMENT

# SPIKING DAN DESPIKING

Berdasarkan buku testing goat SPIKING merupakan sebuah hal yang kita lakukan yang sifat nya explatory jika kita ingin mencoba menjalankan API baru serta mencoba-coba solusi baru dengan cepat. Spiking ini bisa kita lakukan tanpa test dengan itu kita bisa melihat hasil dari experimen kita lebih cepat menurut parcival best practice nya ialah kita melakukan sebuah spike ke branch baru dan kembali nge despike di master namun untuk kita kita ke branch baru despike di branhch lain baru di push ke master. Untuk despiking sendiri ialah mengimplentasikan apa yang kita sudah pelajari pada spike kita dan menerapkan konsep konsep TDD pada nya. Hasilnya menurut parcival akan sebuah kode yang lebih baik dan belum tentu sama dengan fase spiking tersebut.

# MANUAL MOCKING DAN MOCK LIBRARY

Manual mocking atau monkey patching adalah metode yang membuat kita melakukan mocking secara manual jadi kita harus menyediakan semua object yang kita ingin virtualisasikan "MOCK" secara sendiri jadi kita harus membuat nya sendiri.

Mock libarary yang ada di python sejak python 3.3 (menurut parcival pada testing goat) ialah sebuah libarary yang bisa membantu kita pada proses membuat mock tersebut jadi object yang kita panggil tidak akan object sebenernya melainkan sebuah mock alias sebuah object yang di pakai khusus untuk testing dan tidak tergaet dengan objek yang asli yang ada.


# MANUAL MOCKING DAN MOCK LIBRARY

Manual mocking atau monkey patching adalah metode yang membuat kita melakukan mocking secara manual jadi kita harus menyediakan semua object yang kita ingin virtualisasikan "MOCK" secara sendiri jadi kita harus membuat nya sendiri.

Mock libarary yang ada di python sejak python 3.3 (menurut parcival pada testing goat) ialah sebuah libarary yang bisa membantu kita pada proses membuat mock tersebut jadi object yang kita panggil tidak akan object sebenernya melainkan sebuah mock alias sebuah object yang di pakai khusus untuk testing dan tidak tergaet dengan objek yang asli yang ada.

# TEST FIXTURE , DECORATORS for Explicit wait

Untuk kali ini saya menambahkan Decorators serta FT namun masih error dikarenakan ada error pada django saya dan untuk sekarang baru sampai sini saya akan cari lagi pembenarannya dan membenarkan untuk week depan
