from .base import FunctionalTest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class NewVisitorTest(FunctionalTest):
    def test_see_profile(self):
        # Mr.X wants to see ahmad yazid's home page . He goes
        # to chech out its homepage
        self.browser.get('http://localhost:8000')
        # He notices the page title and header has Ahmad Yazid's name and npm.
        self.assertIn('Ahmad Yazid Homepage', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Ahmad Yazid', header_text)
        subheader_text = self.browser.find_element_by_tag_name('h2').text
        self.assertIn('1506722752', subheader_text)
        # Satisfied, he found what he was looking for Mr.x went to sleep

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get(self.live_server_url)

        header_text = self.browser.find_element_by_id('to_do_header').text
        self.assertIn('To-Do', header_text)

        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(inputbox.get_attribute('placeholder'),
                         'Enter a to-do item'
                         )

        inputbox.send_keys('Buy peacock feathers')
        inputbox.send_keys(Keys.ENTER)
        import time
        time.sleep(3)
        # give time for test to find element
        check_list_url = self.browser.current_url
        self.assertRegex(check_list_url, '/lists/.+')
        self.check_for_row_in_list_table('1:Buy peacock feathers')

        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Use peacock feathers to make a fly')
        inputbox.send_keys(Keys.ENTER)
        time.sleep(3)

        self.check_for_row_in_list_table('1:Buy peacock feathers')
        self.check_for_row_in_list_table(
            '2:Use peacock feathers to make a fly')

        self.browser.quit()
        self.browser = webdriver.Firefox()

        # end of mr.x list
        time.sleep(3)
        # mr.y comes into site wants to make list
        # mr.y dont see mr.x list
        self.browser.get(self.live_server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy peacock feathers', page_text)
        self.assertNotIn('make a fly', page_text)

        # Mr.y starts a new list by entering a new item. He
        # is less interesting than mr.x ...
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Buy milk')
        inputbox.send_keys(Keys.ENTER)
        time.sleep(3)

        # Mr.Y gets his own unique URL
        francis_list_url = self.browser.current_url
        self.assertRegex(francis_list_url, '/lists/.+')
        self.assertNotEqual(francis_list_url, check_list_url)
        # Again, there is no trace of Mr.x's list
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy peacock feathers', page_text)
        self.assertIn('Buy milk', page_text)
        # Satisfied, they both go back to sleep
