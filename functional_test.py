from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest


class NewVisitorTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')

    def test_see_profile(self):
        # Mr.X wants to see ahmad yazid's home page . He goes
        # to chech out its homepage
        self.browser.get('http://localhost:8000')
        # He notices the page title and header has Ahmad Yazid's name and npm.
        self.assertIn('Ahmad Yazid Homepage', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Ahmad Yazid', header_text)
        subheader_text = self.browser.find_element_by_tag_name('h2').text
        self.assertIn('1506722752', subheader_text)
        # Satisfied, he found what he was looking for Mr.x went to sleep

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://localhost:8000')

        header_text = self.browser.find_element_by_id('to_do_header').text
        self.assertIn('To-Do', header_text)

        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(inputbox.get_attribute('placeholder'),
                         'Enter a to-do item'
                         )

        inputbox.send_keys('Buy peacock feathers')
        inputbox.send_keys(Keys.ENTER)
        import time
        time.sleep(3)
        # give time for test to find element
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.check_for_row_in_list_table('1:Buy peacock feathers')

        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Use peacock feathers to make a fly')
        inputbox.send_keys(Keys.ENTER)
        time.sleep(3)

        self.check_for_row_in_list_table('1:Buy peacock feathers')
        self.check_for_row_in_list_table(
            '2:Use peacock feathers to make a fly')
        self.fail('finish the test')


if __name__ == '__main__':
    unittest.main(warnings='ignore')
