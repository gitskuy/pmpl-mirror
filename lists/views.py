from django.shortcuts import redirect, render
from lists.models import Item, List
from django.http import HttpResponse
# Create your views here.


def home_page(request):
    item = Item()
    items = Item.objects.all()
    comment = get_comment(items.count())
    return render(request, 'homepage.html', {'comment': comment})


def get_comment(count):
    if count == 0:
        comment = "yey, waktunya berlibur"
    elif count < 5:
        comment = "sibuk tapi santai"
    else:
        comment = "oh tidak"
    return comment


def view_list(request, list_id):
    list_ = List.objects.get(id=list_id)
    item = Item()
    items = Item.objects.filter(list_id=list_id)
    print(items.count())
    comment = get_comment(items.count())
    return render(request, 'list.html', {'list': list_, 'comment': comment})


def new_list(request):
    list_ = List.objects.create()
    Item.objects.create(text=request.POST['item_text'], list=list_)
    return redirect('/lists/%d/' % (list_.id,))


def add_item(request, list_id):
    list_ = List.objects.get(id=list_id)
    Item.objects.create(text=request.POST['item_text'], list=list_)
    return redirect('/lists/%d/' % (list_.id,))
