from django.urls import resolve
from django.test import TestCase
from lists.views import home_page
from lists.models import Item, List
from django.shortcuts import render
from django.http import HttpRequest
from django.template.loader import render_to_string
# Create your tests here.


class HomePageTest(TestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        self.assertIn(b'<title>Ahmad Yazid Homepage</title>',
                      response.content)
        self.assertIn(b'<h1>Ahmad Yazid</h1>',
                      response.content)
        self.assertIn(b'<h2>1506722752</h2>',
                      response.content)

    # def test_home_page_can_save_a_POST_request(self):
    #     request = HttpRequest()
    #     request.method = 'POST'
    #     request.POST['item_text'] = 'A new list item'

    #     response = home_page(request)

    #     self.assertEqual(Item.objects.count(), 1)
    #     new_item = Item.objects.first()
    #     self.assertEqual(new_item.text, 'A new list item')

    # def test_home_page_redirects_after_post(self):
    #     request =HttpRequest()
    #     request.method  = 'POST'
    #     request.POST['item_text'] = 'A new list item'

    #     response = home_page(request)

    #     self.assertEqual(response.status_code, 302)
    #     self.assertEqual(response['location'],
    #                      '/lists/the-only-list-in-the-world')

    def test_home_page_only_saves_items_when_necessary(self):
        request = HttpRequest()
        home_page(request)
        self.assertEqual(Item.objects.count(), 0)

    def test_home_page_right_comments_when_zero(self):
        request = HttpRequest()
        new_list = List.objects.create()
        response = self.client.get('/lists/%d/' % (new_list.id,))
        self.assertIn(b'<h1 id="comment">yey, waktunya berlibur</h1>',
                      response.content)

    def test_home_page_right_comments_when_less_then_5(self):
        request = HttpRequest()
        new_list = List.objects.create()
        Item.objects.create(text='itemey 1', list=new_list)
        Item.objects.create(text='itemey 2', list=new_list)
        Item.objects.create(text='itemey 3', list=new_list)
        Item.objects.create(text='itemey 4', list=new_list)
        response = self.client.get('/lists/%d/' % (new_list.id,))
        self.assertIn(b'<h1 id="comment">sibuk tapi santai</h1>',
                      response.content)

    def test_home_page_right_comments_when__5_or_more(self):
        request = HttpRequest()
        new_list = List.objects.create()
        Item.objects.create(text='itemey 1', list=new_list)
        Item.objects.create(text='itemey 2', list=new_list)
        Item.objects.create(text='itemey 3', list=new_list)
        Item.objects.create(text='itemey 4', list=new_list)
        Item.objects.create(text='itemey 5', list=new_list)
        response = self.client.get('/lists/%d/' % (new_list.id,))
        self.assertIn(b'<h1 id="comment">oh tidak</h1>',
                      response.content)

    def test_kill_firstmutant(self):
        request = HttpRequest()
        new_list = List.objects.create()
        Item.objects.create(text='itemey 1', list=new_list)
        Item.objects.create(text='itemey 2', list=new_list)
        Item.objects.create(text='itemey 3', list=new_list)
        Item.objects.create(text='itemey 4', list=new_list)
        Item.objects.create(text='itemey 5', list=new_list)
        Item.objects.create(text='itemey 6', list=new_list)
        response = self.client.get('/lists/%d/' % (new_list.id,))
        self.assertIn(b'<h1 id="comment">oh tidak</h1>',
                      response.content)

    def test_kill_secondMutant(self):
        request = HttpRequest()
        new_list = List.objects.create()
        Item.objects.create(text='itemey 1', list=new_list)
        Item.objects.create(text='itemey 2', list=new_list)
        Item.objects.create(text='itemey 3', list=new_list)
        Item.objects.create(text='itemey 4', list=new_list)
        Item.objects.create(text='itemey 5', list=new_list)
        response = self.client.get('/lists/%d/' % (new_list.id,))
        self.assertIn(b'<h1 id="comment">oh tidak</h1>',
                      response.content)


class ListViewTest(TestCase):
    def test_uses_list_template(self):
        list_ = List.objects.create()
        response = self.client.get('/lists/%d/' % (list_.id,))
        self.assertTemplateUsed(response, 'list.html')

    def test_displays_only_items_for_that_list(self):
        correct_list = List.objects.create()
        Item.objects.create(text='itemey 1', list=correct_list)
        Item.objects.create(text='itemey 2', list=correct_list)
        other_list = List.objects.create()
        Item.objects.create(text='other list item 1', list=other_list)
        Item.objects.create(text='other list item 2', list=other_list)
        response = self.client.get('/lists/%d/' % (correct_list.id,))
        self.assertContains(response, 'itemey 1')
        self.assertContains(response, 'itemey 2')
        self.assertNotContains(response, 'other list item 1')
        self.assertNotContains(response, 'other list item 2')

    def test_passes_correct_list_to_template(self):
        other_list = List.objects.create()
        correct_list = List.objects.create()
        response = self.client.get('/lists/%d/' % (correct_list.id,))
        self.assertEqual(response.context['list'], correct_list)


class NewListTest(TestCase):
    def test_saving_a_POST_request(self):
        self.client.post(
            '/lists/new',
            data={'item_text': 'A new list item'}
        )
        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new list item')

    def test_redirects_after_POST(self):
        response = self.client.post(
            '/lists/new',
            data={'item_text': 'A new list item'}
        )
        new_list = List.objects.first()
        self.assertRedirects(response, '/lists/%d/' % (new_list.id,))


class NewItemTest(TestCase):
    def test_can_save_a_POST_request_to_an_existing_list(self):
        other_list = List.objects.create()
        correct_list = List.objects.create()
        self.client.post(
            '/lists/%d/add_item' % (correct_list.id,),
            data={'item_text': 'A new item for an existing list'}
        )
        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new item for an existing list')
        self.assertEqual(new_item.list, correct_list)

    def test_redirects_to_list_view(self):
        other_list = List.objects.create()
        correct_list = List.objects.create()
        response = self.client.post(
            '/lists/%d/add_item' % (correct_list.id,),
            data={'item_text': 'A new item for an existing list'}
        )
        self.assertRedirects(response, '/lists/%d/' % (correct_list.id,))
